<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('feedback');
});

Route::post('feedback', function (Request $request) {
    // return $request->all();
    $data = collect([
        'customer_id'          => $request->input('customerID'),
        'first_name'           => $request->input('firstName'),
        'surname'              => $request->input('surname'),
        'dob'                  => $request->input('dob'),
        'gender'               => $request->input('gender'),
        'purchased_product'    => $request->input('hasPurchasedProduct')
                                  && $request->input('productPurchased')
            ? $request->input('productPurchased')
            : 'null',
        'product_satisfaction' => $request->input('hasPurchasedProduct')
                                  && $request->input('productPurchased')
                                  && $request->input('productSatisfaction')
            ? $request->input('productSatisfaction')
            : 'null',
        'purchased_service'    => $request->input('hasPurchasedService')
                                  && $request->input('servicePurchased')
            ? $request->input('servicePurchased')
            : 'null',
        'service_satisfaction' => $request->input('hasPurchasedService')
                                  && $request->input('servicePurchased')
                                  && $request->input('serviceSatisfaction')
            ? $request->input('serviceSatisfaction')
            : 'null',
        'timestamp'            => \Carbon\Carbon::now()->toDateTimeString(),
    ]);

    return print_r($data->toArray(), true);
    // return json_encode($data, JSON_PRETTY_PRINT);
});
