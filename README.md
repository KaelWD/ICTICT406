# ICTICT406 Create User Documentation

## Environment setup

 - Windows or linux
 - PHP >=7.0 with composer
 - Node.js >=7.5.0 with NPM
 - Yarn >=0.24 (`npm install yarn --global`)

Clone the repo:

    git clone https://gitlab.com/KaelWD/ICTICT406.git

Initialise composer:

    composer create-project

Install packages:

    yarn install
    
Build resources:

    npm run production
    
Start development server:

    php artisan serve --host=[HOST] --port=[PORT]
    
## Copyright

The contents of this repository are licensed under the MIT license. 

The Laravel framework is open-sourced software licensed under the MIT license.

Vue.js is open-sourced software licensed under the MIT license.

Twitter Bootstrap is licensed under the MIT License.
