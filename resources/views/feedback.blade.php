<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div id="app" class="container" v-cloak>
    <header class="page-header row align-items-end">
        <div class="col-4"><img src="{{ asset('/shop-logo.png') }}" alt="shop! logo"></div>
        <div class="col-8"><h1>Customer feedback</h1></div>
    </header>
    <form action="{{ url('/feedback') }}" method="post" @submit.prevent="formSubmitted()">

        <div class="form-group row">
            <label for="idInput" class="col-4 col-form-label">Customer ID</label>
            <div class="col-8">
                <input type="text"
                       pattern="[A-Z]{3}[0-9]{3}"
                       class="form-control"
                       id="idInput"
                       placeholder="ABC123"
                       v-model="customerID"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label for="firstNameInput" class="col-4 col-form-label">Name</label>
            <div class="row no-gutters col-8">

                <div class="col-6">
                    <input type="text"
                           pattern="[a-zA-Z0-9]+"
                           class="form-control"
                           id="firstNameInput"
                           placeholder="First"
                           v-model="firstName"
                           required>
                </div>

                <div class="col-6">
                    <input type="text"
                           pattern="[a-zA-Z0-9]+"
                           class="form-control"
                           id="surnameInput"
                           placeholder="Last"
                           v-model="surname"
                           required>
                </div>

            </div>
        </div>

        <div class="form-group row">
            <label for="dobInput" class="col-4 col-form-label">Date of Birth</label>
            <div class="col-8">
                <input type="date" class="form-control" id="dobInput" v-model="dob" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="genderInput" class="col-4 col-form-label">Gender</label>
            <div class="col-8">
                <select id="genderInput" class="custom-select" v-model="gender" required>
                    <option value="" disabled>Select a gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-4 col-form-label">Purchased</label>
            <div class="col-8">

                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" v-model="hasPurchasedProduct">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Product</span>
                </label>

                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" v-model="hasPurchasedService">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Service</span>
                </label>

            </div>
        </div>

        <div class="form-group row" v-if="hasPurchasedProduct">
            <label for="productPurchased" class="col-4 col-form-label">Product Purchased</label>
            <div class="col-8">
                <select id="productPurchased" class="custom-select" v-model="productPurchased" required>
                    <option value="" disabled>Select a product</option>
                </select>
            </div>
        </div>

        <div class="form-group row" v-if="hasPurchasedService">
            <label for="servicePurchased" class="col-4 col-form-label">Service Purchased</label>
            <div class="col-8">
                <select id="servicePurchased" class="custom-select" v-model="servicePurchased" required>
                    <option value="" disabled>Select a service</option>
                    <option value="repairs">Repairs</option>
                    <option value="advice">Advice</option>
                    <option value="cleaning">Cleaning</option>
                    <option value="gardening">Gardening</option>
                    <option value="landscaping">Landscaping</option>
                </select>
            </div>
        </div>

        <div class="form-group row" v-if="hasPurchasedProduct && productPurchased">
            <label class="col-4 col-form-label">
                How satisfied were you with the @{{ productPurchased }} product?
            </label>
            <div class="col-8">
                <label class="custom-control custom-radio" v-for="n in 10">
                    <input type="radio"
                           name="satisfaction"
                           :value="n - 1"
                           class="custom-control-input"
                           v-model="productSatisfaction"
                           required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">@{{ n - 1 }}</span>
                </label>
            </div>
        </div>

        <div class="form-group row" v-if="hasPurchasedService && servicePurchased">
            <label class="col-4 col-form-label">
                How satisfied were you with the @{{ servicePurchased }} service?
            </label>
            <div class="col-8">
                <label class="custom-control custom-radio" v-for="n in 10">
                    <input type="radio"
                           name="satisfaction"
                           :value="n - 1"
                           class="custom-control-input"
                           v-model="serviceSatisfaction"
                           required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">@{{ n - 1 }}</span>
                </label>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <div class="modal fade" ref="responseModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Saved to database</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <pre ref="responseModalBody"></pre>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" ref="errorModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Error</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <pre ref="errorModalBody"></pre>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="{{ mix('/js/app.js') }}"></script>

</body>
</html>
