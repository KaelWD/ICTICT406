
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

const app = new Vue({
    el: '#app',
    data: {
        customerID: '',
        firstName: '',
        surname: '',
        dob: '',
        gender: '',
        hasPurchasedProduct: false,
        productPurchased: '',
        productSatisfaction: false,
        hasPurchasedService: false,
        servicePurchased: '',
        serviceSatisfaction: false,
    },
    methods: {
        formSubmitted() {
            axios.post('/feedback', this.$data).then(({data}) => {
                this.$refs.responseModalBody.innerText = data;
                $(this.$refs.responseModal).modal('show');
            }, err => {
                console.error(err);
                this.$refs.errorModalBody.innerText = err.message;
                $(this.$refs.errorModal).modal('show');
            });
        }
    },
});
